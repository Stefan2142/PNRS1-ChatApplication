# PNRS1
Project from class: ["Projektovanje namenskih računarskih struktura 1"](http://www.rt-rk.uns.ac.rs/predmeti/e2/pnrs-1-projektovanje-namenskih-ra%C4%8Dunarskih-struktura-1)

# Task
Create a simple chat application where each phone is a client connected to the server so users can chat with other registered users.

# Features
1. Log in/Register new user
2. Client side authentication with the server
3. Encryption of messages
4. SQLite to store the messages

# About
Created for Android API 21
